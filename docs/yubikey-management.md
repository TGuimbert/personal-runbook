# Yubikey Management

Useful commands to manage an Yubikey 5.

## Generate SSH key

Create a ED25519 key pair that require the security key to be plugged and that
ask for the key's PIN each use:

```shell
ssh-keygen -t ed25519-sk -O verify-required
```

!!! bug

    The option "verify-required" is not yet supported by gnome-keyring,
    for more information, see the [issue](https://gitlab.gnome.org/GNOME/gnome-
    keyring/-/issues/101).

    A workaround is to rename the public key file:
 
    ```shell
    mv ~/.ssh/id_ed25519_sk.pub ~/.ssh/id_ed25519_sk.pub.hidden
    ```