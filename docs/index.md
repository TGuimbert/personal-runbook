# Welcome to my Personnal Runbook

This is a collection of things I want to recall or need to do regularly. It might contain instructions specific to my environment (mainly Fedora and Fedora Silverblue), I can't guaranty it will work for everyone but feel free to take inspiration from it.

This static site is generated thanks to MKDocs Material, for full documentation visit [mkdocs.org](https://squidfunk.github.io/mkdocs-material/).

## Commands

* `podman run --rm -it -p 8000:8000 -v ./:/docs squidfunk/mkdocs-material` - Start the live-reloading docs server.
* `podman run --rm -it -v ./:/docs:z squidfunk/mkdocs-material build` - Build the documentation site.

## Project layout

    mkdocs.yml    # The configuration file.
    docs/
        index.md  # The documentation homepage.
        ...       # Other markdown pages, images and other files.
