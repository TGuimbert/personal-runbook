# Workstation Management

## Install Ansible

1. Install `pip`:

  ``` shell
  python -m ensurepip --upgrade
  ```

1. Verify that `pip` is installed:

  ``` shell
  python3 -m pip -V
  ```

1. Install `ansible`:

  ``` shell
  python3 -m pip install --user ansible
  ```

1. Verify that `ansible` is installed:

  ``` shell
  ansible --version
  ```

1. Run the playbook:

  ``` shell
  ansible-playbook site.yaml
  ```
